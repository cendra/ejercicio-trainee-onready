package vista;

import controlador.Controlador;

public class main {

	public static void main(String[] args) 
	{
		Controlador controlador = new Controlador();
		
		controlador.inicializar();
		
		controlador.mostrarVehiculo();
		controlador.imprimirSeparador();
		controlador.mostrarVehiculoMasCaro();
		controlador.mostrarVehiculoMasBarato();
		controlador.mostrarPorLetraY();
		controlador.imprimirSeparador();
		controlador.mostrarPorMayorAMenorPrecio();
		
		

	}

}
