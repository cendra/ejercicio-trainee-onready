package modelo;

public class Marca 
{
	
	private String nombre;
	
	public Marca(String nuevaMarca)
	{
		nombre = nuevaMarca;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
