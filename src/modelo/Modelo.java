package modelo;

public class Modelo 
{
	private Marca marca;
	private String nombreModelo;
	
	public Modelo(String nombre, Marca asignarMarca)
	{
		this.nombreModelo = nombre;
		this.marca = asignarMarca;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public String getNombreModelo() {
		return nombreModelo;
	}

	public void setNombreModelo(String nombreModelo) {
		this.nombreModelo = nombreModelo;
	}
	
	
	
}
