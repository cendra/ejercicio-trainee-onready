package controlador;

import modelo.Marca;
import modelo.Modelo;
import modelo.Vehiculo;

public class Controlador 
{
	private Vehiculo[] vehiculos;
	private Modelo[] modelos;
	
	public Controlador()
	{	
		vehiculos = new Vehiculo[4];
	}
	
	public void mostrarVehiculo()
	{
		boolean conPuerta = false;
		for(int i=0;i < vehiculos.length;i++)
		{
			if(conPuerta)
			{
				System.out.println("Marca: " + this.vehiculos[i].getModelo().getMarca().getNombre()+" // "+"Modelo: "+this.vehiculos[i].getModelo().getNombreModelo()+" // "+" Cilindrada: "+ this.vehiculos[i].getCilindraje()+" // "+" Precio: $"+this.vehiculos[i].getPrecio());
				conPuerta = false;
			}
			else
			{
				System.out.println("Marca: " + this.vehiculos[i].getModelo().getMarca().getNombre()+" // "+"Modelo: "+this.vehiculos[i].getModelo().getNombreModelo()+" // "+" Puertas: "+ this.vehiculos[i].getCantidadPuertas()+" // "+" Precio: $"+this.vehiculos[i].getPrecio());
				conPuerta = true;
			}
		}
	}
	
	public void mostrarPorLetraY()
	{
		for(int i =0;i<this.vehiculos.length;i++)
		{
			if(empiezaConY(this.vehiculos[i].getModelo().getNombreModelo()))
			{
				System.out.println("Veh�culo que contiene en el modelo la letra �Y�:" + this.vehiculos[i].getModelo().getMarca().getNombre()+" " + this.vehiculos[i].getModelo().getNombreModelo() + "  $" +this.vehiculos[i].getPrecio());
			}
		}
	}
	
	private boolean empiezaConY(String nombreModelo) 
	{
		if(nombreModelo.charAt(0) == 'Y' || nombreModelo.charAt(0) == 'y')
			return true;
		else
			return false;
	}

	private  Vehiculo obtenerVehiculoMasCaro()
	{
		double max = 0;
		Modelo mod = new Modelo("", new Marca(""));
		Vehiculo maxVehiculo = new Vehiculo(mod, 0, "", 0.0);
		
		for(int i =0;i<this.vehiculos.length;i++)
		{
			if(this.vehiculos[i].getPrecio() > max)
			{
				max = this.vehiculos[i].getPrecio();
				maxVehiculo = this.vehiculos[i];
			}
		}
		return maxVehiculo;
	}
	
	public void mostrarVehiculoMasCaro()
	{
		Vehiculo vehiculo = obtenerVehiculoMasCaro();
		System.out.println("Veh�culo m�s caro: "+ vehiculo.getModelo().getMarca().getNombre() + " " + vehiculo.getModelo().getNombreModelo());
	}
	
	public void mostrarVehiculoMasBarato()
	{
		double min = 1000000000; 
		Modelo mod = new Modelo("", new Marca(""));
		Vehiculo minVehiculo = new Vehiculo(mod, 0, "", 0.0);
		
		for(int i =0;i<this.vehiculos.length;i++)
		{
			if(this.vehiculos[i].getPrecio() < min)
			{
				min = this.vehiculos[i].getPrecio();
				minVehiculo = this.vehiculos[i];
			}
		}
		System.out.println("Veh�culo m�s barato: "+ minVehiculo.getModelo().getMarca().getNombre() + " " +minVehiculo.getModelo().getNombreModelo());
	}
	
	private void ordenarPorMayorAMenorPrecio()
	{
		for (int i = 0; i < vehiculos.length; i++) {
	        for (int j = 0; j < vehiculos.length-i-1; j++) {
	            if(this.vehiculos[j].getPrecio() < vehiculos[j+1].getPrecio()){
	                Vehiculo tmp = vehiculos[j+1];
	                vehiculos[j+1] = this.vehiculos[j];
	                this.vehiculos[j] = tmp;
	            }
	        }
		}
	}
	
	public void mostrarPorMayorAMenorPrecio()
	{
		System.out.println("Cehiculos ordenados por precio de mayor a menor:/n");
		this.ordenarPorMayorAMenorPrecio();
		for(int i =0 ; i < vehiculos.length; i++)
		{
			System.out.println(this.vehiculos[i].getModelo().getMarca().getNombre() + " " + this.vehiculos[i].getModelo().getNombreModelo() +"/n");
		}
	}
	
	public void inicializar()
	{
		this.cargarModelos();
		
		this.vehiculos[0] = new Vehiculo(modelos[0], 4, "134cc", 200000.00);
		this.vehiculos[1] = new Vehiculo(modelos[1], 3, "125cc", 60000.00);
		this.vehiculos[2] = new Vehiculo(modelos[2], 5, "130cc", 250000.00);
		this.vehiculos[3] = new Vehiculo(modelos[3], 2, "160cc", 80500.50);
		
	}
	
	
	private void cargarModelos()
	{
		modelos = new Modelo[4];
		
		modelos[0] = new Modelo("206", new Marca("Peugeot"));
		modelos[1] = new Modelo("Titan", new Marca("Honda"));
		modelos[2] = new Modelo("208", new Marca("Peugeot"));
		modelos[3] = new Modelo("YBR", new Marca("Yamaha"));
		
	}	
	
	public void imprimirSeparador()
	{
		System.out.println("=============================");
	}
	
	
}
